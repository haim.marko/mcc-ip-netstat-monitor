<#
.SYNOPSIS

.DESCRIPTION

.EXAMPLE

.LINK

#>

Param (
    [Parameter(Mandatory=$True)]
    [String]$Cluster1,
    [Parameter(Mandatory=$True)]
    [String]$Cluster2,        
    [Parameter(Mandatory=$False)]
    [ValidateRange(5,7000)]
    [Int]$Interval=10,
    [Parameter(Mandatory=$False)]
    [String]$LogFilePath,
    [Parameter(Mandatory=$False)]
    [switch]$Monitor=$False  	
)

#Get current date 
$BaseTime = (Get-Date)

#Netstat command 
$NetstatCmd = "set -confirmation off;set d; systemshell * -command sudo jexec 5 netstat -scnp tcp"

#will be used as a parameter for Write-Log to specifiy and error 
$E = $True


#Retrasmit threshold 
$RetransmitPercentTH = 0.01

function Write-Log ($log,$e,$notime){
    if ($log) {
        if (!$notime) {
            $log = "$(get-date) - $log" # <--- Prompt to screen.
        }
        if ($e) {
            Write-Host "ERROR: $log" -ForegroundColor Red
            if ($LogFilePath) {"ERROR: $log" | Out-File $LogFilePath -Append}
            exit(1)
        } else {
            Write-Host "$log"
			if ($LogFilePath) { $log | Out-File $LogFilePath -Append}
        }
    }
}

#Load DataOntap module 
$module = Get-Module DataONTAP
if ($module -eq $null) {
    $module = Import-Module -Name DataONTAP -PassThru
    if ( $module -eq $null ) {
        Write-Log "DataONTAP PowerShell module not found or cannot be loaded" $E
        
    }
}


$a = Get-NcCredential -name $Cluster1 -ErrorVariable Err -ErrorAction SilentlyContinue -OutVariable Cred1
if (-not $Cred1 -or $Err) {
    Write-Log "Could not find cached credenitals for cluster: $Cluster1, plese use Add-NCCredential Cmdlet to cache it" $E
}

$a = Get-NcCredential -name $Cluster2 -ErrorVariable Err -ErrorAction SilentlyContinue -OutVariable Cred2
if (-not $Cred2 -or $Err) {
    Write-Log "Could not find cached credenitals for cluster: $Cluster2, plese use Add-NCCredential Cmdlet to cache it " $E
}

Write-Log "Starting data collection"

$C1 = Connect-NcController -name $Cluster1 -ErrorVariable Err
if ($Err){
    Write-Log "Could not connect NetApp cluster: $Cluster1 - $($Err.Exception)" $E
}

$C2 = Connect-NcController -name $Cluster2 -ErrorVariable Err
if ($Err){
    Write-Log "Could not connect NetApp cluster: $Cluster2 - $($Err.Exception)" $E
}

$PrevNetstat = @{}

#Will contain node names
$C1N1 = ''; $C1N2 = ''; $C2N1 = ''; $C2N2 = ''

while ($True) {
	
	#This Parmeter will be set as True if retransmit threshhold breached (more than $RetransmitPercentTH)
	$RetransmitIssue = $False
	
	#hash for netstat output 
	$Netstat = @{}

	$Netstat.add($Cluster1, $(Invoke-NcSsh -Name $C1 -Command $NetstatCmd))
	#StartTime cluster1
	$StartTime1 = (Get-Date).Ticks 	
	
	$Netstat.add($Cluster2, $(Invoke-NcSsh -Name $C2 -Command $NetstatCmd))
	#StartTime cluster2 
	$StartTime2 = (Get-Date).Ticks 
	
	#Initialize data structure for parsed Netstat data 
	$NetstatData = @{}
	
	ForEach ($Cluster in $Netstat.keys) {
		$ns = $Netstat[$Cluster]
		ForEach ($line in $($ns -split "`n"))
		{
		
			if ($line -match "Invalid argument") {
				Write-Log "Could not get netstat information for MCC IP " $E
			}
			if ($Line -match "^Node\:\s+(?<Node>.+)\s*") {
				$Node = $Matches.Node					
				if ($Cluster -eq $Cluster1) {
					$NetstatData[$Cluster+'^'+$Node+'^'+'Time'] = $StartTime1
				} else {
					$NetstatData[$Cluster+'^'+$Node+'^'+'Time'] = $StartTime2
				}
			}                              
			if ($Line -match "^\s+(?<Packets>\d+) packets sent") {
				$NetstatData[$Cluster+'^'+$Node+'^'+'PacketsSent'] = $Matches.Packets 
			}                                              
			if ($Line -match "^\s+(?<Packets>\d+) data packets \((?<Bytes>\d+) bytes\)\s*$") {
				$NetstatData[$Cluster+'^'+$Node+'^'+'BytesSent'] = $Matches.Bytes 
			}  

			if ($Line -match "^\s+(?<Packets>\d+) data packets \((?<Bytes>\d+) bytes\) retransmitted") {                                                                
				$NetstatData[$Cluster+'^'+$Node+'^'+'PacketsRetransmited'] = $Matches.Packets 
				$NetstatData[$Cluster+'^'+$Node+'^'+'BytesRetransmited'] = $Matches.Bytes 
			}                              

			if ($Line -match "^\s+(?<Packets>\d+) packets received\s*$") {
				$NetstatData[$Cluster+'^'+$Node+'^'+'PacketsReceived'] = $Matches.Packets 
			}                              
		
			if ($Line -match "^\s+\d+ packets \((?<Bytes>\d+) bytes\) received in-sequence\s*$") {
				$NetstatData[$Cluster+'^'+$Node+'^'+'BytesReceived'] = $Matches.Bytes 
			}                         			

			if ($Line -match "^\s+(?<Packets>\d+) out-of-order packets \((?<Bytes>\d+) bytes\)") {     
				$NetstatData[$Cluster+'^'+$Node+'^'+'PacketsOOO'] = $Matches.Packets 
				$NetstatData[$Cluster+'^'+$Node+'^'+'BytesOOO'] = $Matches.Bytes 
			}
			if ($Line -match "^\s+(?<Packets>\d+)  completely duplicate packets \((?<Bytes>\d+) bytes\)") {                                                              
				$NetstatData[$Cluster+'^'+$Node+'^'+'PacketsDup'] = $Matches.Packets
				$NetstatData[$Cluster+'^'+$Node+'^'+'BytesDup'] = $Matches.Bytes
			}              
			if ($Line -match "^\s+(?<ECN>\d+) times ECN reduced the congestion window") {
				$NetstatData[$Cluster+'^'+$Node+'^'+'ECN'] = $Matches.ECN
			}                                              
		}                              
	}

	if ($PrevNetstat.Count -gt 0) {
		$Time = 0
		ForEach ($Key in $NetstatData.keys) { 
			$Cluster, $Node, $Counter = $key.Split('^')
			
			$Delta = $NetstatData[$Cluster+'^'+$Node+'^'+$Counter] - $PrevNetstat[$Cluster+'^'+$Node+'^'+$Counter]
			
			if ($Counter.StartsWith('Bytes') -and $Delta -gt 0) {
				$Delta = [math]::Round($Delta/1MB,2)
			}
			
			if ($Counter -eq "BytesSent") {
				$Time = ($NetstatData[$Cluster+'^'+$Node+'^'+'Time'] - $PrevNetstat[$Cluster+'^'+$Node+'^'+'Time'])/10000000
				$MBSent = [math]::Round($Delta/$Time,2)
				switch ($Node) {
					$C1N1 {$Results."$C1N1 Sent" = [string]$MBSent+"MB/s"}
					$C1N2 {$Results."$C1N2 Sent" = [string]$MBSent+"MB/s"}
					$C2N1 {$Results."$C2N1 Sent" = [string]$MBSent+"MB/s"}
					$C2N2 {$Results."$C2N2 Sent" = [string]$MBSent+"MB/s"}
				}
			}
			
			if ($Counter -eq "BytesReceived") {
				$Time = ($NetstatData[$Cluster+'^'+$Node+'^'+'Time'] - $PrevNetstat[$Cluster+'^'+$Node+'^'+'Time'])/10000000
				$MBReceived = [math]::Round($Delta/$Time,2)
				switch ($Node) {
					$C1N1 {$Results."$C1N1 Received" = [string]$MBReceived+"MB/s"}
					$C1N2 {$Results."$C1N2 Received" = [string]$MBReceived+"MB/s"}
					$C2N1 {$Results."$C2N1 Received" = [string]$MBReceived+"MB/s"}
					$C2N2 {$Results."$C2N2 Received" = [string]$MBReceived+"MB/s"}
				}
			}			

			if ($Counter -eq "PacketsRetransmited") {
				$PacketsSent = $NetstatData[$Cluster+'^'+$Node+'^'+"PacketsSent"] - $PrevNetstat[$Cluster+'^'+$Node+'^'+"PacketsSent"]
				#Prevent div 0 
				if ($PacketsSent -eq 0) {
					$PacketsSent = 1
				}
				$RetransPercent = [math]::Round(($Delta/$PacketsSent)*100,6) -as [decimal]
				
				$RetransmitIssueMessage = ''
				if ($RetransPercent -ge $RetransmitPercentTH) {
					$RetransmitIssue = $True
					$RetransmitIssueMessage = " ****** Threshold Breached ******"
				}
				switch ($Node) {
					$C1N1 {$Results."$C1N1 Retransmit %" = [string]$RetransPercent+'%'+$RetransmitIssueMessage}
					$C1N2 {$Results."$C1N2 Retransmit %" = [string]$RetransPercent+'%'+$RetransmitIssueMessage}
					$C2N1 {$Results."$C2N1 Retransmit %" = [string]$RetransPercent+'%'+$RetransmitIssueMessage}
					$C2N2 {$Results."$C2N2 Retransmit %" = [string]$RetransPercent+'%'+$RetransmitIssueMessage}
				}
			}
			
			if ($Counter -eq "ECN") {
				$Time = ($NetstatData[$Cluster+'^'+$Node+'^'+'Time'] - $PrevNetstat[$Cluster+'^'+$Node+'^'+'Time'])/10000000
				$ECNs = [math]::Round($Delta/$Time,0)

				switch ($Node) {
					$C1N1 {$Results."$C1N1 ECN" = [string]$ECNs+'/s'}
					$C1N2 {$Results."$C1N2 ECN" = [string]$ECNs+'/s'}
					$C2N1 {$Results."$C2N1 ECN" = [string]$ECNs+'/s'}
					$C2N2 {$Results."$C2N2 ECN" = [string]$ECNs+'/s'}
				}
			}			
			
			if ($Counter -eq "PacketsOOO") {
				$Time = ($NetstatData[$Cluster+'^'+$Node+'^'+'Time'] - $PrevNetstat[$Cluster+'^'+$Node+'^'+'Time'])/10000000
				$OOOs = [math]::Round($Delta/$Time,0)

				switch ($Node) {
					$C1N1 {$Results."$C1N1 Out Of Order" = [string]$OOOs+'/s'}
					$C1N2 {$Results."$C1N2 Out Of Order" = [string]$OOOs+'/s'}
					$C2N1 {$Results."$C2N1 Out Of Order" = [string]$OOOs+'/s'}
					$C2N2 {$Results."$C2N2 Out Of Order" = [string]$OOOs+'/s'}
				}
			}						
			
		}
		
		#print the results 
		$Results | FL
		
		if ($LogFilePath) {
			$Results | FL | Out-File -Append $LogFilePath
		}
		
		if ($Monitor) {
			if ($RetransmitIssue) {
				Write-Log "Retransmit Threshhold of 0.01% or above been breached on one or more node" E
				exit(1)
			} else {
				exit(0)
			}
		}
		
	}  else {
	#1st iteration build the output object structure 
		#only in the 1st time 
		ForEach ($Key in $NetstatData.keys) { 
			$Cluster, $Node, $Counter = $key.Split('^')
			
			#Declare Node Names
			if ($Cluster -eq $Cluster1 -and $C1N1 -eq '') {
				$C1N1 = $Node
			}
			if ($Cluster -eq $Cluster1 -and $C1N1 -ne '' -and $Node -ne $C1N1 -and $C1N2 -eq '') {
				$C1N2 = $Node 
			} 
			if ($Cluster -eq $Cluster2 -and $C2N1 -eq '') {
				$C2N1 = $Node
			}
			if ($Cluster -eq $Cluster2 -and $C2N1 -ne '' -and $Node -ne $C2N1 -and $C2N2 -eq '') {
				$C2N2 = $Node
			}			
		}
		$Results = ''|  SELECT  "$($C1N1) Sent","$($C1N1) Received","$($C1N1) Retransmit %","$($C1N1) ECN","$($C1N1) Out Of Order",
								"$($C1N2) Sent","$($C1N2) Received","$($C1N2) Retransmit %","$($C1N2) ECN","$($C1N2) Out Of Order",
								"$($C2N1) Sent","$($C2N1) Received","$($C2N1) Retransmit %","$($C2N1) ECN","$($C2N1) Out Of Order",
								"$($C2N2) Sent","$($C2N2) Received","$($C2N2) Retransmit %","$($C2N2) ECN","$($C2N2) Out Of Order"
	
	}
	
	#Clone NetstatData to PrevNetstat to be able to calculate delta 
	$PrevNetstat = $NetstatData.Clone()
	
	#Sleep time between iterations 
	Write-Log "Sleeping $Interval"
	Start-Sleep $Interval
}

